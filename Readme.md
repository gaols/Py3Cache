## Py3Cache

### 简介

该项目是我在学习 Python 的时候，为了避免陷入花很长时间去看完一本编程语言学习书后仍然找不到北的状况。针对自己熟悉的一个小领域，换另外一种新语言实现，在实现中有针对性的学习新的语言。

[Py3Cache](https://git.oschina.net/ld/Py3Cache) 是 [J2Cache](https://git.oschina.net/ld/J2Cache) 两级缓存框架的 Python 语言移植版本。关于两级缓存框架的思路已经要解决的问题请看 [J2Cache](https://git.oschina.net/ld/J2Cache) 项目首页中的文章以及视频，这里不再赘述。

Py3Cache 的一级缓存基于 LRU 算法实现的纯内存缓存 [pylru](https://pypi.python.org/pypi/pylru/) 。二级缓存基于 Redis 存储实现。使用 Redis 的 Pub/Sub 进行缓存事件分发。

### 已存在问题

* 当前缓存数据因过期失效是采取读触发机制，当读取数据时会判断该数据生成时间是否大于有效时间，如果大于有效时间则清除并返回 None。也就是说当一个缓存数据如果一直没有被读取时就会一直存在于内存和Redis之中。

### 软件依赖

* Python 3
* PyRedis （二级缓存）(`pip install redis`)
* Pickle （序列化器）
* Redis （服务）

### 使用方法

1. 安装 Py3Cache(`pip install Py3Cache`)
2. 安装 Redis 服务并启动
3. 修改 config.ini 中关于 Redis 服务的主机和端口的配置（文件位于 site-packages/py3cache 目录）
4. 在 config.ini 的 [memory] 定义一级缓存，具体请看 config.ini 的示例配置（可选）
5. 启动多个测试应用 : python3 Py3Cache.py

### 测试方法

启动测试应用后可以使用如下命令进行缓存的操作

> exit/quit 退出测试应用
> get [region] [key] 读取 [region] 中的 [key] 缓存数据
> set [region] [key] [val] 设置 [region] 中的 [key] 值为 [val]
> evict [region] [key] 清除某个缓存数据
> clear [region] 清除整段缓存数据

### 示例代码

在程序中使用的方法

```
from py3cache import Py3Cache
p3c = Py3Cache.Py3Cache()
p3c.set("sites","oschina",{"id":100,"name":"oschina","url":"https://git.oschina.net"})
p3c.get("sites","oschina")
p3c.evict("sites","oschina")
p3c.close()
```

### 寻求帮助

如果你有什么的建议以及完善的需求，请提交 Issue 或者直接提交 Pull Requests。